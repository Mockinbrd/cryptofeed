import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Button from "../components/_commons/Button";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Example/Button',
  component: Button,
} as ComponentMeta<typeof Button>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof Button> = (args) => <Button {...args}>Children</Button>;

export const TypeButton = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
TypeButton.args = {
  type: "button",
};

export const TypeSubmit = Template.bind({});
TypeSubmit.args = {
  type: "submit",
};

export const TypeReset = Template.bind({});
TypeReset.args = {
  type: "reset",
};
