import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Header from "../components/_commons/Header";
import {BrowserRouter} from "react-router-dom";

export default {
  title: 'Example/Header',
  component: Header,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
  },
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = (args) => <BrowserRouter><Header /></BrowserRouter>;

export const NormalHeader = Template.bind({});
NormalHeader.args = {};
