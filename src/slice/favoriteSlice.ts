import { createSlice } from "@reduxjs/toolkit";

export const favoriteSlice = createSlice({
  name: "favorite",
  initialState: [] as any[],
  reducers: {
    add: (state, action) => {
      for (let element of state) {
        if (element.id === action.payload.id) return state;
      }
      state.push(action.payload);
    },
    toggle: (state, action) => {
      for (let element of state) {
        if (element.id === action.payload.id)
          return state.filter((el) => el.id !== action.payload.id);
      }
      state.push(action.payload);
    },
  },
});

// Action creators are generated for each case reducer function
export const { add, toggle } = favoriteSlice.actions;

export default favoriteSlice.reducer;
