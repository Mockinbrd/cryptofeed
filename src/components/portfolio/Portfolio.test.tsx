import React from "react";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../store";
import Portfolio from "./Portfolio";

describe("Portfolio", () => {
  it("should say there are no favorites", function () {
    render(
      <Provider store={store}>
        <Portfolio />
      </Provider>
    );
    expect(screen.getByTestId("nofavs")).toBeInTheDocument();
  });
});
