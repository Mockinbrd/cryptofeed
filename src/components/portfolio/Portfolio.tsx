import { useAppSelector } from "../../hooks";

function Portfolio() {
  const favorites = useAppSelector((state) => state.favorite);
  return (
    <>
      {favorites.length === 0 ? (
        <div className="flex justify-center py-6" data-testid="nofavs">
          You don't have any favorites yet.
        </div>
      ) : (
        <div className="flex justify-center py-6 font-bold text-xl uppercase tracking-wider">
          Your portfolio
        </div>
      )}
      {favorites.map((coin: any) => (
        <div className="px-16">
          <div className="flex justify-center items-center gap-x-8 border-t border-gray-600 p-8">
            <img
              src={coin.large}
              className="rounded-full h-20 w-20 object-cover"
              alt={`${coin.name} icon`}
            />
            <div className="flex flex-col gap-y-2">
              <h1 className="text-xl font-medium">{`${
                coin.name
              } (${coin.symbol.toUpperCase()})`}</h1>
              <span className="text-3xl font-bold">
                ${new Intl.NumberFormat("en-US").format(coin.usd)}
              </span>
            </div>
          </div>
        </div>
      ))}
    </>
  );
}

export default Portfolio;
