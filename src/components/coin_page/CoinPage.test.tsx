import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import CoinPage from "./CoinPage";
import { Provider } from "react-redux";
import store from "../../store";

describe("Coin Page", () => {
  it("should render the coin page", function () {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CoinPage />
        </MemoryRouter>
      </Provider>
    );
    expect(screen.getByTestId("coin-page-container")).toBeInTheDocument();
  });
});
