export type CoinInfo = {
  id: string;
  name: string;
  symbol: string;
  image: { large: string };
  description: { en: string };
  links: { homepage: string[] };
  market_data: {
    current_price: { usd: number };
    market_cap: { usd: number };
    total_supply: number;
  };
};
