import { useLocation, useNavigate, useParams } from "react-router-dom";
import { useCallback, useEffect, useState } from "react";
import { ArrowLeftIcon, StarIcon } from "@heroicons/react/outline";
import toast from "react-hot-toast";
import { api } from "../_commons/api";
import Button from "../_commons/Button";
import { CoinInfo } from "./CoinPage.types";
import { useDispatch } from "react-redux";
import { favoriteSlice } from "../../slice/favoriteSlice";
import { useAppSelector } from "../../hooks";

function CoinPage(): JSX.Element {
  const [coinInfo, setCoinInfo] = useState<CoinInfo>();
  const dispatch = useDispatch();
  const { id } = useParams<string>();
  const favorites = useAppSelector((state) => state.favorite);

  const isFavorite = favorites.some((item: any) => item.id === id);

  const location = useLocation();
  let navigate = useNavigate();

  const getCoin = useCallback(async (): Promise<void> => {
    const { data } = await api.get(
      `/coins/${id}?localization=false&tickers=false&developer_data=false`
    );
    setCoinInfo(data);
  }, [id]);

  useEffect(() => {
    try {
      getCoin();
    } catch (e: any) {
      toast.error(e.message);
    }
  }, [getCoin]);

  const addToFavorite = (event: any) => {
    event.preventDefault();
    if (coinInfo) {
      const {
        id,
        symbol,
        name,
        image: { large },
        market_data: {
          current_price: { usd },
        },
      } = coinInfo;
      dispatch(favoriteSlice.actions.toggle({ id, symbol, name, large, usd }));
      toast.success(`${name} added to favorites`);
    }
  };

  const onArrowClick = (): void => {
    navigate(`${location.state || "/"}`, {
      state: `/coin/${id}`,
    });
  };

  return (
    <div className="flex flex-col mt-2" data-testid="coin-page-container">
      <div className="flex items-center w-1/2 justify-between mx-auto text-gray-100">
        <Button classes=" hover:text-accent" onclick={onArrowClick}>
          <ArrowLeftIcon className="h-6 w-6" />
        </Button>
        <Button
          classes="hover:text-yellow-500"
          onclick={(e) => addToFavorite(e)}
        >
          {isFavorite ? (
            <StarIcon className="h-6 w-6 fill-yellow-500" />
          ) : (
            <StarIcon className="h-6 w-6 hover:fill-yellow-500" />
          )}
        </Button>
      </div>

      <div className="mt-8 px-8">
        <div className="grid grid-cols-2 place-items-center">
          <img
            src={coinInfo?.image.large}
            className="rounded-full h-20 w-20 object-cover"
            alt={`${coinInfo?.name} icon`}
          />
          <div className="flex flex-col gap-y-2">
            <h1 className="text-xl font-medium">{`${
              coinInfo?.name
            } (${coinInfo?.symbol.toUpperCase()})`}</h1>
            <span className="text-3xl font-bold">
              $
              {coinInfo &&
                new Intl.NumberFormat("en-US").format(
                  coinInfo.market_data.current_price.usd
                )}
            </span>
          </div>
        </div>
      </div>

      <div className="mt-8 px-8">
        <div className="flex flex-col divide-y divide-gray-600">
          <div className="flex justify-between items-center py-2">
            <span>Market cap</span>
            <span>
              $
              {coinInfo &&
                new Intl.NumberFormat("en-US").format(
                  coinInfo.market_data.market_cap.usd
                )}
            </span>
          </div>

          <div className="flex justify-between items-center py-2">
            <span>Total Supply</span>
            <span>
              {coinInfo &&
                new Intl.NumberFormat("en-US").format(
                  Math.round(coinInfo.market_data.total_supply)
                )}{" "}
              unit
            </span>
          </div>
        </div>
      </div>

      <div className="mt-8 px-4">
        <h2 className="text-xl font-semibold tracking-wide mb-2">INFOS</h2>
        <div className="p-4 bg-primary rounded-md border-t border-gray-500">
          <div className="flex flex-col gap-y-2">
            <div className="flex items-center justify-between">
              <h3 className="text-gray-200">Website :</h3>
              <a
                href={coinInfo?.links?.homepage[0]}
                target="_blank"
                title={`Link to ${coinInfo?.name} website`}
                className="text-gray-100 hover:underline"
                rel="noreferrer"
              >
                {coinInfo?.links?.homepage[0]}
              </a>
            </div>
            <h3 className="text-gray-200">Description :</h3>
            <p className="text-gray-400 text-justify">
              {coinInfo?.description?.en}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CoinPage;
