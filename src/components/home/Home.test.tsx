import React from "react";
import { render, screen } from "@testing-library/react";
import Home from "./Home";

it("renders search container", () => {
  render(<Home />);
  const searchContainer = screen.getByTestId("test-search-container");
  expect(searchContainer).toBeInTheDocument();
});
