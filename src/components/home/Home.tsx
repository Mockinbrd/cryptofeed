import { useEffect, useState } from "react";
import { toast } from "react-hot-toast";

import CoinTable from "../coin_table/CoinTable";
import SkeletonLoader from "../coin_table/SkeletonLoader";
import Search from "../search/Search";
import { fetchTrendingCoins } from "./Home.constants";
import { Coin } from "./Home.types";
import { api } from "../_commons/api";
import "../../styles/App.css";

function Home(): JSX.Element {
  const [loading, setLoading] = useState<boolean>(true);
  const [coins, setCoins] = useState<Coin[]>();

  useEffect(() => {
    try {
      getTrendingCoins();
    } catch (e: any) {
      toast.error(e.message);
    } finally {
      setLoading(false);
    }
  }, []);

  const getTrendingCoins = async (): Promise<void> => {
    const coins = await fetchTrendingCoins();
    setCoins(coins);
  };

  const search = async (query: string): Promise<void> => {
    setLoading(true);
    try {
      const { data } = await api.get(`/search?query=${query}`);
      setCoins(data.coins);
    } catch (e: any) {
      toast.error(e.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="flex flex-col">
      <div className="w-full px-4 mt-4" data-testid="test-search-container">
        <Search search={search} />
      </div>
      {loading && (
        <div className="mt-4">
          <SkeletonLoader />
        </div>
      )}
      {coins && !loading && (
        <div className="w-full mt-8 px-4 sm:px-6 lg:px-8">
          <CoinTable coins={coins} />
        </div>
      )}
    </div>
  );
}

export default Home;
