import { api } from "../_commons/api";
import { Coin } from "./Home.types";

export const fetchTrendingCoins = async (): Promise<Coin[]> => {
  let trendingCoins: Coin[] = [];

  const {
    data: { coins },
  } = await api.get("/search/trending");

  for (let { item } of coins) {
    trendingCoins.push(item);
  }

  return trendingCoins;
};
