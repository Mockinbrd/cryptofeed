function TableSkeletonLoader(): JSX.Element {
  return (
    <div
      className="animate-pulse px-2 flex flex-col gap-y-2"
      data-testid="skeleton-container"
    >
      <div className="w-full bg-gray-400 h-16 bg-opacity-50" />
      <div className="w-full bg-gray-400 h-16 bg-opacity-50" />
      <div className="w-full bg-gray-400 h-16 bg-opacity-50" />
      <div className="w-full bg-gray-400 h-16 bg-opacity-50" />
      <div className="w-full bg-gray-400 h-16 bg-opacity-50" />
      <div className="w-full bg-gray-400 h-16 bg-opacity-50" />
      <div className="w-full bg-gray-400 h-16 bg-opacity-50" />
    </div>
  );
}

export default TableSkeletonLoader;
