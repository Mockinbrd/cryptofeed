import React from "react";
import { render, screen } from "@testing-library/react";
import SkeletonLoader from "./SkeletonLoader";

describe("Skeleton Loader", () => {
  it("should render the skeleton loader", function () {
    render(<SkeletonLoader />);
    expect(screen.getByTestId("skeleton-container")).toBeInTheDocument();
  });
});
