import { useNavigate } from "react-router-dom";
import { Coin } from "../home/Home.types";

type CoinTableProps = {
  coins: Coin[];
};

function CoinTable({ coins }: CoinTableProps): JSX.Element {
  let navigate = useNavigate();

  const onRowClick = (id: string): void => {
    navigate(`/coin/${id}`, {
      state: "/",
    });
  };
  return (
    <div className="-mx-3 overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
      <table
        className="min-w-full divide-y divide-gray-300"
        data-testid="table"
      >
        <thead className="bg-primary">
          <tr>
            <th
              scope="col"
              className="py-3.5 px-3 text-left text-sm font-semibold sm:pl-6"
            >
              #
            </th>

            <th
              scope="col"
              className="py-3.5 pr-3 pl-4 text-left text-sm font-semibold"
            >
              Coin
            </th>
            <th
              scope="col"
              className="px-3 py-3.5 text-left text-sm font-semibold"
            >
              Abbr.
            </th>
            <th
              scope="col"
              className="px-3 py-3.5 text-left text-sm font-semibold"
            >
              MarketCap
            </th>
          </tr>
        </thead>
        <tbody className="divide-y divide-gray-500">
          {coins.map((coin, index) => (
            <tr
              key={coin.id}
              onClick={() => onRowClick(coin.id)}
              className="cursor-pointer hover:bg-primary"
              data-testid={`row-${index}`}
            >
              <td className="px-3 py-4 text-sm text-gray-500">{index}</td>
              <td className="w-full max-w-0 py-4 pl-4 pr-3 text-sm font-medium sm:w-auto sm:max-w-none sm:pl-6">
                <div className="flex coins-center gap-x-2">
                  <img
                    className="h-10 w-10"
                    src={coin.large || coin.thumb || ""}
                    alt={`Icon ${coin.name}`}
                  />
                  <span className="text-lg">{coin.name}</span>
                </div>
              </td>
              <td className="px-3 py-4 text-sm text-gray-500 text-right">
                {coin.symbol}
              </td>
              <td className="px-3 py-4 text-sm text-right">
                {coin.market_cap_rank}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default CoinTable;
