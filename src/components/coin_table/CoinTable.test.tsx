import React from "react";
import { MemoryRouter } from "react-router-dom";
import CoinTable from "./CoinTable";
import { render, screen } from "@testing-library/react";

describe("CoinTable", () => {
  it("does not render row without at least one coin", () => {
    render(
      <MemoryRouter>
        <CoinTable coins={[]} />
      </MemoryRouter>
    );
    expect(screen.queryByTestId("row-1")).not.toBeInTheDocument();
  });
  it("should render the table head", function () {
    render(
      <MemoryRouter>
        <CoinTable coins={[]} />
      </MemoryRouter>
    );
    expect(screen.getByTestId("table")).toBeInTheDocument();
  });
});
