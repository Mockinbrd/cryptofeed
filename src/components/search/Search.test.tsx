import React from "react";
import { render, screen } from "@testing-library/react";
import Search from "./Search";

describe("Portfolio", () => {
  it("should say there are no favorites", function () {
    render(
      <Search
        search={function (value) {
          return;
        }}
      />
    );
    expect(screen.getByTestId("search-container")).toBeInTheDocument();
  });
});
