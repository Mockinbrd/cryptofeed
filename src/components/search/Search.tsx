import { SearchIcon } from "@heroicons/react/outline";
import { KeyboardEvent, useState } from "react";
import { toast } from "react-hot-toast";

type SearchProps = {
  search: (value: string) => void;
};

function Search({ search }: SearchProps): JSX.Element {
  const [value, setValue] = useState<string>("");

  const onKeyPressed = (e: KeyboardEvent<HTMLInputElement>): void => {
    if (e.key === "Enter" && value.length > 3) {
      return search(value);
    }
    toast("Please type at least 4 characters in the search bar");
  };

  return (
    <div
      className="relative rounded-md shadow-sm"
      data-testid="search-container"
    >
      <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
        <SearchIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
      </div>
      <input
        type="text"
        name="search"
        id="search"
        value={value}
        onChange={(e) => setValue(e.target.value)}
        placeholder="bitcoin"
        className="block w-full pl-10 h-10 bg-primary border border-gray-600 rounded-md"
        autoComplete="off"
        onKeyPress={(event) => onKeyPressed(event)}
      />
    </div>
  );
}

export default Search;
