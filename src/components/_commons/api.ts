import axios, { AxiosInstance } from "axios";

export const api: AxiosInstance = axios.create({
  baseURL:
    process.env.REACT_APP_COIN_GECKO_API ?? "https://api.coingecko.com/api/v3",
});
