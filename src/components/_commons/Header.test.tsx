import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import Header from "./Header";

it("renders search container", () => {
  render(
    <MemoryRouter>
      <Header />
    </MemoryRouter>
  );
  const logo = screen.getByText(/CryptoFeed/i);
  expect(logo).toBeInTheDocument();
});
