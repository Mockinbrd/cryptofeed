import { CollectionIcon } from "@heroicons/react/outline";
import { Link } from "react-router-dom";

import Logo from "./Logo";

function Header(): JSX.Element {
  return (
    <div className="flex justify-between items-center px-4 py-4 gap-x-2">
      <Link to={""}>
        <div className="flex gap-x-2 items-center">
          <Logo classes="h-10" />
          <span className="font-semibold text-2xl tracking-wider">
            CryptoFeed
          </span>
        </div>
      </Link>
      <Link to="portfolio">
        <CollectionIcon className="w-8 h-8 ml-auto cursor-pointer hover:text-accent" />
      </Link>
    </div>
  );
}

export default Header;
