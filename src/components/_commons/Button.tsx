interface ButtonProps {
  children: any;
  onclick?: (event?: any) => void;
  classes?: string;
  type?: "submit" | "button" | "reset";
}

const Button = ({
  children,
  onclick,
  classes = "",
  type = "button",
}: ButtonProps) => {
  return (
    <button
      type={type}
      className={`px-4 py-1 rounded-lg font-semibold ${classes}`}
      onClick={onclick}
    >
      {children}
    </button>
  );
};

export default Button;
