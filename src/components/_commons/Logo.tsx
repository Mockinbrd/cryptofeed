function Logo({ classes = "" }: { classes?: string }) {
  return (
    <img
      className={`${classes} object-cover`}
      src="/logo192.png"
      alt="Logo Cryptofeed"
      data-testid="logo-img"
    />
  );
}

export default Logo;
