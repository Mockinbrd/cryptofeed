import React from "react";
import { render, screen } from "@testing-library/react";
import Logo from "./Logo";

describe("Logo", () => {
  it("should render the logo img", function () {
    render(<Logo />);
    expect(screen.getByTestId("logo-img")).toBeInTheDocument();
  });
});
